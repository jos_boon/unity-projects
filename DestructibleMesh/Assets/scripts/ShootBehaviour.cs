﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootBehaviour : MonoBehaviour {

	private Transform bullet;
	public Transform shootStart;

	void Awake()
	{
		bullet = Resources.Load<Transform>("Prefabs/bullet");
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown("Fire1")) {
			Transform clone = Instantiate(bullet, shootStart.position, shootStart.rotation);
			Rigidbody rb = clone.GetComponent<Rigidbody>();
			rb.AddForce(shootStart.transform.forward * 1000);
		}
	}
}
