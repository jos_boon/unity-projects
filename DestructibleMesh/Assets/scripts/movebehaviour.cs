﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movebehaviour : MonoBehaviour {

	Transform t;
	// Use this for initialization
	void Start () {
		t = Camera.main.transform;
	}

	void FixedUpdate() {
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");

		t.position += new Vector3(x, y, 0);
	}
}
