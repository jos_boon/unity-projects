﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour 
{
	private static GameManager instance;
	private float respawnTimer = 0;

	public float respawnDelay = 3f;
	public GameState gameState { get; set; }
	public Transform spawnPoint;
	public Ship player;
	public GameObject pMenu;
	private Text txtMenu;

	public static GameManager Instance
	{
		get 
		{
			if(instance == null)
				instance = FindObjectOfType<GameManager>();

			return instance;
		}
	}

	void Awake()
	{
		//There can only be one!
		if(Instance != this)
			Destroy(gameObject);
	}

	void Start()
	{
		if(spawnPoint == null)
			spawnPoint = GameObject.Find("SpawnPoint").transform;

		if(player == null)
			player = GameObject.FindGameObjectWithTag("Player").GetComponent<Ship>();

		if(pMenu == null)
			pMenu = GameObject.Find("pMenu");

		txtMenu = GameObject.Find("txtGameMenu").GetComponent<Text>();

		GamePause();
	}

	void Update()
	{
		if(gameState == GameState.Respawn && respawnTimer >= respawnDelay)
			ResetPlayer();
		else if(gameState == GameState.Respawn)
			respawnTimer += Time.deltaTime;
	}

	public void ResumeGame()
	{
		Time.timeScale = 1;
		pMenu.SetActive(false);
	}

	public void GamePause(bool openMenu = true)
	{
		Time.timeScale = 0;
		txtMenu.text = "Game Paused";
		pMenu.SetActive(openMenu);
	}

	public void RestartGame()
	{
		//TODO: Set this to level one
		Application.LoadLevel("EndlessLevel");
		ResumeGame();
	}

	public void ExitGame()
	{
		Application.Quit();
	}

	public void GameOver()
	{
		GamePause();
		txtMenu.text = "Game Over!";
		GameObject.Find("btnResume").SetActive(false);
	}

	public void StartRespawn()
	{
		gameState = GameState.Respawn;
		player.gameObject.SetActive(false);
		respawnTimer = 0;
	}

	public void ResetPlayer()
	{
		player.gameObject.SetActive(true);
		player.transform.position = spawnPoint.position;
		player.Initialize();
		gameState = GameState.Play;
	}
	
	public enum GameState
	{
		Play,
		Pause,
		Respawn,
		GameOver
	}
}
