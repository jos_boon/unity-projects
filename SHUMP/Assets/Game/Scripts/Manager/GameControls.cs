﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameControls : MonoBehaviour 
{
	private GameManager gm;
	public GameObject pMenu;

	void Start()
	{
		gm = GameManager.Instance;
	}

	void Update() 
	{
		if(gm.gameState == GameManager.GameState.Play &&(Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)))
			gm.GamePause();
		else if(gm.gameState == GameManager.GameState.Pause &&(Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape)))
			gm.ResumeGame();
	}
}
