﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour
{
	public int MaxParts = 0;
	public int minPartsToShop = 5;
	public int maxPartsToShop = 5;
	private int outcomeForShop = 0;
	private int partsPassed = 0;

	private GameObject current;
	private GameObject next;

	private static LevelGenerator instance;

	public static LevelGenerator Instance
	{
		get
		{
			if(instance == null)
				instance = FindObjectOfType<LevelGenerator>();

			return instance;
		}
	}

	void Awake()
	{
		if(Instance != this)
			Destroy(gameObject);
	}

	// Create 2 starting Parts
	void Start()
	{
		current = Resources.Load("Prefabs/LevelParts/levelPart00", typeof(GameObject)) as GameObject;
		current = Instantiate(current, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
		Camera.main.GetComponent<SideScrolling>().next = current.transform.GetComponentInChildren<CheckPointBehaviour>().transform;

		RandomizeShopPart();
		partsPassed++;

		InstantiateNextPart();
	}

	public void InstantiateNextPart()
	{
		if(partsPassed == outcomeForShop)
		{
			RandomizeShopPart();
			next = Resources.Load("Prefabs/LevelParts/levelPartShop") as GameObject;
		}
		else
		{
			int n = Random.Range(0, MaxParts);
			next = Resources.Load("Prefabs/LevelParts/levelPart" + n.ToString("00")) as GameObject;
			partsPassed++;
		}

		next = Instantiate(next, current.transform.position + new Vector3(64, 0, 0), Quaternion.identity) as GameObject;

		setCheckPoint();

		current = next;
	}

	private void RandomizeShopPart()
	{
		outcomeForShop = Random.Range(minPartsToShop, maxPartsToShop);
		partsPassed = 0;
	}

	public void setCheckPoint()
	{
		CheckPointBehaviour cpb = current.transform.GetComponentInChildren<CheckPointBehaviour>();
		CheckPointBehaviour nCpb = next.transform.GetComponentInChildren<CheckPointBehaviour>();
		cpb.next = nCpb.transform;
	}
}
