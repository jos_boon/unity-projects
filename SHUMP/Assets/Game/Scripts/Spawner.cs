﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour 
{
	public GameObject[] spawnObjects;
	public Vector3 minSpawn;
	public bool canSpawn = true;
	public int spawnCount = 0;
	public float delay = 1;

	private Vector3 position;
	private Vector3 minPosition;
	private int spawns = 0;

	void Start () 
	{
		position = transform.position;
		minPosition = position - minSpawn;

		if(spawnObjects.Length == 0)
			Destroy(gameObject);

		if(canSpawn)
			InvokeRepeating("SpawnEnemies", 0.1f, delay);
	}

	private void SpawnEnemies()
	{
		spawns++;
		Vector3 spawn = position;

		spawn.x = Random.Range(minPosition.x, position.x);
		spawn.y = Random.Range(minPosition.y, position.y);

		if(spawnObjects.Length == 1)
		{
			GameObject spawnObj = Instantiate(spawnObjects[0], spawn, spawnObjects[0].transform.rotation) as GameObject;
			spawnObj.transform.SetParent(transform);
		}
		else
		{
			int i = Random.Range(0, spawnObjects.Length);
			GameObject spawnObj = Instantiate(spawnObjects[i], spawn, spawnObjects[i].transform.rotation) as GameObject;
			spawnObj.transform.SetParent(transform);
		}

		if(spawns >= spawnCount)
			CancelInvoke();
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player"))
		{
			if(!canSpawn)
			{
				canSpawn = true;
				InvokeRepeating("SpawnEnemies", 0.1f, delay);
			}
				
		}
	}
}
