﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour 
{
	public float invincibleInterval = 2f; //-1 for no delay

	private ShipState shipState;
	private float invincibleTimer = 0f;
	public bool isInvincible { get; private set; }
	private ScrapDrop scrap;

	public ShipState State
	{
		get { return shipState; }
		set
		{
			shipState = value;

			if(shipState == ShipState.Death)
			{
				ResourceManager.Instance.Respawn(this);

				if(scrap)
					scrap.dropScrap();
			}
		}
	}

	void Awake()
	{
		for(int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).tag = tag;
		}
	}

	void Start () 
	{
		scrap = GetComponent<ScrapDrop>();
		Initialize();
	}

	public void Initialize()
	{
		GetComponentInChildren<BodyBehaviour>().Initialize();
		shipState = ShipState.Alive;
		invincibleTimer = 0f;
		isInvincible = true;
	}

	void Update()
	{
		if(isInvincible && invincibleInterval != -1)
		{
			invincibleTimer += Time.deltaTime;

			if(invincibleTimer >= invincibleInterval)
				isInvincible = false;
		}
	}

	public void DestroyShip()
	{
		Destroy(gameObject);
	}
}

public enum ShipState
{
	Alive,
	Death
}
