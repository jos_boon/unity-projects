﻿using UnityEngine;
using System.Collections;

public class UserControls : MonoBehaviour {
	private MoveBehaviour move;
	private ShootingBehaviour shoot;

	void Awake()
	{
		move = GetComponentInChildren<MoveBehaviour>();
		shoot = GetComponentInChildren<ShootingBehaviour>();
	}
	
	void Update () 
	{
		if(Input.GetButton("ShipFire"))
		{
			shoot.Shoot();
		}
	}

	void FixedUpdate()
	{
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");

		move.Movement(x, y);
	}

}
