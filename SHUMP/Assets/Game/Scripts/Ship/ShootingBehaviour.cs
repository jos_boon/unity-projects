﻿using UnityEngine;
using System.Collections;

public class ShootingBehaviour : MonoBehaviour 
{
	public GameObject Projectile;
	public Transform ShootTrans;
	public float Speed = 200;
	public float shootInterval = .1f; //Timer between shots;
	public int damage = 1;
	public AttributeBehaviour attBeh;
	
	private Vector2 direction;
	private bool canShoot = true;

	void Start()
	{
		if(CompareTag("Player"))
			Projectile = Resources.Load("Prefabs/projectilePlayer", typeof(GameObject)) as GameObject;
		else if(CompareTag("Enemy"))
			Projectile = Resources.Load("Prefabs/projectileEnemy", typeof(GameObject)) as GameObject;
		
		if(!Projectile)
		{
			print("Failed to load in Projectile!!\n DESTROYING OBJECT...");
			Destroy(gameObject);
		}

		calculateDirection();
	}
	
	public void Shoot()
	{
		if(isActiveAndEnabled && canShoot)
		{
			GameObject g = Instantiate(Projectile, ShootTrans.position, ShootTrans.rotation) as GameObject;
			Rigidbody2D r = g.GetComponent<Rigidbody2D>();
			g.GetComponent<ProjectileBehaviour>().Damage = damage;

			r.AddForce(direction * Speed);

			if(shootInterval > 0)
			{
				canShoot = false;
				Invoke("resetCanShoot", shootInterval);
			}
		}
	}

	public void calculateDirection()
	{
		direction = ShootTrans.position - transform.position;
		direction.Normalize();
	}

	public void resetCanShoot()
	{
		canShoot = true;
	}
}
