﻿using UnityEngine;
using System.Collections;

public class DestroyProjectile : MonoBehaviour 
{
	public float AliveTimer = 4;

	void Start()
	{
		Invoke("destroyObject", AliveTimer);
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		//TODO: Look at script OnHitBehaviour (line 38)
		if(other.tag == "Environment")
			Destroy(gameObject);
	}

	private void destroyObject()
	{
		Destroy(gameObject);
	}
}
