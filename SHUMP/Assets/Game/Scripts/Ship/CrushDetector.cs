﻿using UnityEngine;
using System.Collections;

public class CrushDetector : MonoBehaviour 
{
	private Ship ship;
	private BodyBehaviour body;

	void Start()
	{
		ship = GetComponentInParent<Ship>();
		body = ship.GetComponentInChildren<BodyBehaviour>();
	}

	void OnTriggerStay2D(Collider2D other)
	{
		//print(other.gameObject.tag);
		string otherTag = other.gameObject.tag;

		//TODO: What if we want ship crushers?
		if(string.Equals(otherTag, "Environment") && ship.State == ShipState.Alive)
		{
			body.setDamage(int.MaxValue);
			//Destroy(gameObject.transform.parent.gameObject);
		}
	}
}
