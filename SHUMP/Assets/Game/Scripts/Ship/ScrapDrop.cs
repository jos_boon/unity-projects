﻿using UnityEngine;
using System.Collections;

public class ScrapDrop : MonoBehaviour 
{
	public ScrapBehaviour scrap;
	public int ScrapValue = 2;
	public float DropChange;

	public void dropScrap()
	{
		if(Random.value <= DropChange)
		{
			scrap.ScrapValue = ScrapValue;
			Instantiate(scrap, transform.position, Quaternion.identity);
		}
	}
}
