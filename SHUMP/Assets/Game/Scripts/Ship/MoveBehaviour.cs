﻿using UnityEngine;
using System.Collections;

public class MoveBehaviour : MonoBehaviour {

	public float speed = 5;
	public AttributeBehaviour attBeh;

	private Rigidbody2D m_Rigidbody2D;
	// Use this for initialization

	void Awake()
	{
		m_Rigidbody2D = GetComponentInParent<Rigidbody2D>();
	}

	public void Movement(float x, float y)
	{
		m_Rigidbody2D.velocity = new Vector2(x, y) * speed;
	}
}
