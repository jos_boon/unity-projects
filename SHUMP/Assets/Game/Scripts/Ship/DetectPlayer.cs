﻿using UnityEngine;
using System.Collections;

public class DetectPlayer : MonoBehaviour 
{
	private AIBehaviour ai;

	void Start()
	{
		ai = transform.parent.GetComponent<AIBehaviour>();

		if(!ai)
			Destroy(gameObject);
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player"))
			ai.Target = other.gameObject.transform;
	}
	
	void OnTriggerExit2D(Collider2D other)
	{
		if(other.CompareTag("Player"))
			ai.Target = null;
	}
}
