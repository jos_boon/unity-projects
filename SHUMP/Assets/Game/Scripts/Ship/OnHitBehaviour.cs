﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BodyBehaviour))]
public class OnHitBehaviour : MonoBehaviour 
{
	private BodyBehaviour body;

	void Awake()
	{
		body = GetComponent<BodyBehaviour>();
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		string otherTag = other.gameObject.tag;

		if((tag == "Player" && otherTag == "Enemy") || (tag == "Enemy" && otherTag == "Player"))
		{
			body.setDamage(10);
			print ("Player collides with Enemy!");
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		string otherTag = other.gameObject.tag;

		if(otherTag == "ProjectilePlayer" || otherTag == "ProjectileEnemy")
		{
			ProjectileBehaviour projectile = other.GetComponent<ProjectileBehaviour>();
			if(tag == "Player" && otherTag == "ProjectileEnemy")
			{
				body.setDamage(projectile.Damage);
				//TODO: Think of a beter solution for when projectiles needs to be destroyed
				Destroy(other.gameObject);
			}
			else if(tag == "Enemy" && otherTag == "ProjectilePlayer")
			{
				body.setDamage(projectile.Damage);
				ResourceManager.Instance.Score += 10;
				//TODO: Think of a beter solution for when projectiles needs to be destroyed
				Destroy(other.gameObject);
			}
		}


	}
}
