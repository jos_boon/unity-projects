﻿using UnityEngine;
using System.Collections;

public class ChaseBehaviour : MonoBehaviour {

	private MoveBehaviour move;
	private AIBehaviour ai;
		
	void Start () 
	{
		move = GetComponentInChildren<MoveBehaviour>();
		ai = GetComponent<AIBehaviour>();
	}
	
	void FixedUpdate()
	{
		if(ai.Target)
			move.Movement(transform.right.x, transform.right.y);
		else
			move.Movement(0, 0);
	}
}
