﻿using UnityEngine;
using System.Collections;

public class AIBehaviour : MonoBehaviour 
{
	public Transform Target;
	public bool canShoot = false;
	private ShootingBehaviour shoot;

	void Start () 
	{
		shoot = GetComponentInChildren<ShootingBehaviour>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Target && canShoot)
			shoot.Shoot();
	}

	void FixedUpdate()
	{
		if(!Target)
			return;

		Vector3 direction = (Target.position - transform.position).normalized;
		float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
		Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);

		if(transform.rotation != q)
		{
			transform.rotation = q;
			shoot.calculateDirection();
		}
	}
}
