﻿using UnityEngine;
using System.Collections;

public class MoveAndShoot : MonoBehaviour 
{
	[Range(-1,1)]public int x = 0;
	[Range(-1,1)]public int y = 0;

	private ShootingBehaviour shoot;
	private MoveBehaviour move;

	void Start () 
	{
		shoot = GetComponentInChildren<ShootingBehaviour>();
		move = GetComponentInChildren<MoveBehaviour>();
	}

	void Update () 
	{
		shoot.Shoot();
	}

	void FixedUpdate()
	{
		move.Movement(x, y);
	}
}
