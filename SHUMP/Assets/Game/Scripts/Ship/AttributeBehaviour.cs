﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AttributeBehaviour 
{
	public int increaseStatBy = 1;
	public int iLevel = 1;
	public int maxLevel = 10;
	public int baseCost = 10;
	public int costMultiplier = 1;

	public int CostOfNextLevel
	{
		get 
		{
			return baseCost * costMultiplier * (iLevel + 1);
		}
	}

}
