﻿using UnityEngine;
using System.Collections;

public class BodyBehaviour : MonoBehaviour 
{
	public int maxHitPoints = 1;
	public int MaxHitPoints
	{
		get { return maxHitPoints; }
		set
		{
			if(hitPoints == maxHitPoints)
				HitPoints = maxHitPoints = value;
			else
				maxHitPoints = value;
		}
	}

	private int hitPoints;
	public int HitPoints
	{
		get { return hitPoints; }
		set 
		{
			hitPoints = value;

			if(hitPoints > MaxHitPoints)
				hitPoints = maxHitPoints;

			if(CompareTag("Player"))
				ResourceManager.Instance.UpdateHpHud(hitPoints);
		}
	}

	public AttributeBehaviour attBeh;
	private Ship ship;
	
	void Start () 
	{
		Initialize();
		ship = GetComponentInParent<Ship>();
	}

	public void Initialize()
	{
		HitPoints = MaxHitPoints;
	}

	public void setDamage(int damage)
	{
		if(ship.isInvincible)
			return;
		
		HitPoints -= damage;
		
		if(HitPoints <= 0)
			ship.State = ShipState.Death;
	}
}
