﻿using UnityEngine;
using System.Collections;

public class ScrapBehaviour : MonoBehaviour 
{
	public int ScrapValue = 2;

	void Start()
	{
		Rigidbody2D rb = GetComponent<Rigidbody2D>();

		rb.AddTorque(60f);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player"))
		{
			ResourceManager.Instance.Scraps += ScrapValue;
			Destroy(gameObject);
		}
	}
	
}
