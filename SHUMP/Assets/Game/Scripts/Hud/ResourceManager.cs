﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResourceManager : MonoBehaviour 
{
	public int lives = 3;
	public int score = 0;
	public int scraps = 0;

	private Text txtLives;
	private Text txtScore;
	private Text txtScraps;
	private Text txtHpHud;

	private static ResourceManager instance;

	public static ResourceManager Instance
	{
		get
		{
			if(instance == null)
				instance = FindObjectOfType<ResourceManager>();

			return instance;
		}
	}

	public int Lives
	{
		get { return lives; }
		set 
		{ 
			lives = value;

			if(txtLives)
			{
				txtLives.text = "Lives: " + (lives > 0 ? lives : 0);
			}
				
		}
	}

	public int Score
	{
		get { return score; }
		set 
		{ 
			score = value;

			if(txtScore)
				txtScore.text = "Score: " + score;
		}
	}

	public int Scraps
	{
		get { return scraps; }
		set 
		{ 
			scraps = value;

			if(txtScraps)
				txtScraps.text = "Scraps: " + scraps;
		}
	}

	void Awake()
	{
		//There can only be one!
		if(Instance != this)
			Destroy(gameObject);

		GameObject obj;

		obj = GameObject.Find("txtLives");
		if(obj)
			txtLives = obj.GetComponent<Text>();
		
		obj = GameObject.Find("txtScore");
		if(obj)
			txtScore = obj.GetComponent<Text>();
		
		obj = GameObject.Find("txtScraps");
		if(obj)
			txtScraps = obj.GetComponent<Text>();
		
		obj = GameObject.Find("txtHpHud");
		if(obj)
			txtHpHud = obj.GetComponent<Text>();
		
		if(!txtLives || !txtScore || !txtScraps)
			print ("Text Component(lives, score and/or scraps) missing!");
	}

	void Start()
	{
		// Force Update HUD
		Lives = lives;
		Score = score;
		Scraps = scraps;
	}

	public void Respawn(Ship ship)
	{
		if(!ship.CompareTag("Player"))
		{
			ship.DestroyShip();
			return;
		}

		Lives--;

		if(Lives < 0)
			GameManager.Instance.GameOver();
		else
			GameManager.Instance.StartRespawn();


	}

	public void UpdateHpHud(int hp)
	{
		txtHpHud.text = "Hp: " + hp;
	}
}
