﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopUIBehaviour : MonoBehaviour 
{
	private ResourceManager resMan;
	private MoveBehaviour playerMove;
	private ShootingBehaviour playerShoot;
	private BodyBehaviour playerBody;

	private Text txtHp;
	private Text txtHpScrap;
	private Text txtSpd;
	private Text txtSpdScrap;
	private Text txtDmg;
	private Text txtDmgScrap;
	private ErrorTextHandler eth;

	private static ShopUIBehaviour instance;

	public static ShopUIBehaviour Instance
	{
		get
		{
			if(instance == null)
				instance = FindObjectOfType<ShopUIBehaviour>();

			return instance;
		}
	}

	void Awake()
	{
		//There can only be one!
		if(Instance != this)
			Destroy(gameObject);
	}

	// Use this for initialization
	void Start () 
	{
		resMan = ResourceManager.Instance;
		Transform player = GameObject.Find("pShip").transform;

		playerMove = player.GetComponentInChildren<MoveBehaviour>();
		playerShoot = player.GetComponentInChildren<ShootingBehaviour>();
		playerBody = player.GetComponentInChildren<BodyBehaviour>();

		txtHp = GameObject.Find("txtHp").GetComponent<Text>();
		txtHpScrap = GameObject.Find("txtHpScrap").GetComponent<Text>();
		txtSpd = GameObject.Find("txtSpd").GetComponent<Text>();
		txtSpdScrap = GameObject.Find("txtSpdScrap").GetComponent<Text>();
		txtDmg = GameObject.Find("txtDmg").GetComponent<Text>();
		txtDmgScrap = GameObject.Find("txtDmgScrap").GetComponent<Text>();
		eth = GameObject.Find("txtError").GetComponent<ErrorTextHandler>();

		updateUI();

		gameObject.SetActive(false);
	}

	private void updateUI()
	{
		txtHp.text = "Hp:\t\t" + playerBody.attBeh.iLevel + "/" + playerBody.attBeh.maxLevel;			
		txtHpScrap.text = playerBody.attBeh.CostOfNextLevel.ToString();
		txtSpd.text = "Spd:\t\t" + playerMove.attBeh.iLevel + "/" + playerMove.attBeh.maxLevel;	
		txtSpdScrap.text = playerMove.attBeh.CostOfNextLevel.ToString();	
		txtDmg.text = "Dmg:\t" + playerShoot.attBeh.iLevel + "/" + playerShoot.attBeh.maxLevel;		
		txtDmgScrap.text = playerShoot.attBeh.CostOfNextLevel.ToString();
	}

	public void UpgradeHp()
	{
		if(playerBody.attBeh.CostOfNextLevel <= resMan.Scraps)
		{
			playerBody.MaxHitPoints += playerBody.attBeh.increaseStatBy;
			resMan.Scraps -= playerBody.attBeh.CostOfNextLevel;
			playerBody.attBeh.iLevel++;
			
			updateUI();
		}
		else
			eth.ShowError();
	}

	public void UpgradeSpd()
	{ 
		if(playerMove.attBeh.iLevel == playerMove.attBeh.maxLevel)
			return;

		if(playerMove.attBeh.CostOfNextLevel <= resMan.Scraps)
		{
			playerMove.speed += playerMove.attBeh.increaseStatBy;
			resMan.Scraps -= playerMove.attBeh.CostOfNextLevel;
			playerMove.attBeh.iLevel++;

			updateUI();
		}
		else
			eth.ShowError();
	}

	public void UpgradeDmg()
	{
		if(playerShoot.attBeh.CostOfNextLevel <= resMan.Scraps)
		{
			playerShoot.damage += playerShoot.attBeh.increaseStatBy;
			resMan.Scraps -= playerShoot.attBeh.CostOfNextLevel;
			playerShoot.attBeh.iLevel++;
			
			updateUI();
		}
		else
			eth.ShowError();
	}

	public void Repair(int scrapcost = 30)
	{
		if(scrapcost <= resMan.Scraps && playerBody.HitPoints < playerBody.maxHitPoints)
		{
			playerBody.HitPoints++;
			resMan.Scraps -= scrapcost;
		}
		else
			eth.ShowError();
	}

	public void BuyLife(int scrapcost = 200)
	{
		if(scrapcost <= resMan.Scraps)
		{
			resMan.Lives++;
			resMan.Scraps -= scrapcost;
		}
		else
			eth.ShowError();
	}

	public void ExitShop()
	{
		gameObject.SetActive(false);
		GameManager.Instance.ResumeGame();
	}
}
