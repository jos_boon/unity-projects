﻿using UnityEngine;
using System.Collections;

public class ShopBehaviour : MonoBehaviour 
{
	bool justEntered = false;

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag("Player") && !justEntered)
		{
			GameManager.Instance.GamePause(false);
			ShopUIBehaviour.Instance.gameObject.SetActive(true);

			justEntered = true;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.CompareTag("Player"))
		{
			justEntered = false;
		}
	}
}
