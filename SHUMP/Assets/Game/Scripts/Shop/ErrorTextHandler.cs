﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ErrorTextHandler : MonoBehaviour {

	private Text txtError;
	
	void Start () 
	{
		txtError = GetComponent<Text>();
		HideError();
	}
	
	public void ShowError(string t = "")
	{
		if(!string.IsNullOrEmpty(t))
			txtError.text = t;

		txtError.enabled = true;

		Invoke("HideError", 5);
	}

	public void HideError()
	{
		txtError.enabled = false;
	}
}
