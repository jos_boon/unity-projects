﻿using UnityEngine;
using System.Collections;

public class StayInBounds2D : MonoBehaviour 
{
	public Transform target;
	private SpriteRenderer spRender;
	
	void Start () 
	{
		if(target == null)
			target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();

		// Make player child of the camera for the side scrolling 
		target.SetParent(transform);

		spRender = target.GetComponentInChildren<SpriteRenderer>();
	}


	void LateUpdate()
	{
		Vector3 cMin = Camera.main.ViewportToWorldPoint(Vector3.zero);
		Vector3 cMax = Camera.main.ViewportToWorldPoint(Vector3.one);

		float xTarget = target.position.x;
		float yTarget = target.position.y;
		float zTarget = target.position.z;

		if(spRender.bounds.min.x <= cMin.x)
			xTarget = cMin.x + spRender.bounds.extents.x;
		else if(spRender.bounds.max.x >= cMax.x)
			xTarget = cMax.x - spRender.bounds.extents.x;

		if(spRender.bounds.min.y <= cMin.y)
			yTarget = cMin.y + spRender.bounds.extents.y;
		else if(spRender.bounds.max.y >= cMax.y)
			yTarget = cMax.y - spRender.bounds.extents.y;

		target.position = new Vector3(xTarget, yTarget, zTarget);
	}

}
