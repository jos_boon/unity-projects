﻿using UnityEngine;
using System.Collections;

public class CheckPointBehaviour : MonoBehaviour 
{
	public Transform next;

	public void MarkForDestroy()
	{
		Invoke("DestroyLevel", 20);
	}

	private void DestroyLevel()
	{
		Destroy(transform.parent.gameObject);
	}

}
