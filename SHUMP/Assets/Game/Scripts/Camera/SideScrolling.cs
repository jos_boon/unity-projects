﻿using UnityEngine;
using System.Collections;

public class SideScrolling : MonoBehaviour {

	public Transform next;
	public float Speed = 1;
	public float snapValue = .1f;
	private Vector3 from;
	private float startTime;
	private float journeyLength;
	private Vector3 prevPosition;

	void Start()
	{
		setNextTarget(next);

		prevPosition = transform.position;
	}
	
	void LateUpdate()
	{
		if(!next)
			return;
			
		float distCovered = (Time.time - startTime) * Speed;
		float fracJourney = distCovered / journeyLength;

		transform.position = Vector3.Lerp(from, next.position, fracJourney);

		//Snap camera to checkpoint and look for next target
		if(Vector3.Distance(transform.position, next.position) <= snapValue)
		{
			transform.position = next.position;
			CheckPointBehaviour cpb = next.GetComponent<CheckPointBehaviour>();

			if(cpb)
			{
				cpb.MarkForDestroy();
				setNextTarget(cpb.next);
			}
			else
				next = null;
		}
	}

	public void setNextTarget(Transform n)
	{
		next = n;

		if(!next)
			return;

		startTime = Time.time;
		from = transform.position;
		journeyLength = Vector3.Distance(from, next.position);

		LevelGenerator.Instance.InstantiateNextPart();

	}
}
