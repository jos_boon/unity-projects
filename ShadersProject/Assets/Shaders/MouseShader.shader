﻿Shader "Hidden/MouseShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			fixed4 _point;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv);
				fixed2 coord = i.uv;
				fixed2 dist = coord - _point;

				// BullsEye
				col.r = abs(coord.x - length(dist));
				col.g = abs(coord.y - length(dist));
				col.b = 1 - (0.5 + 0.5*sin(length(dist) * 20));

				// SpotLight
				// Use 1 - Length to reverse the colours
				// White > Dark
				//col.rgb = col.rgb - length(dist);

				// White > Yellow
				//col.rg = 1;
				//col.b = length(dist); // 1 - length to inverse it

				//col.rgb = length(coord - 0.5);
				//col.a = 0;

				//Rainbow like
				//col.rg = coord;
				//col.b = length(dist);

				return col;
			}
			ENDCG
		}
	}
}
