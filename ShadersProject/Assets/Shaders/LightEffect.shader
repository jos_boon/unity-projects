﻿Shader "Hidden/LightEffect"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
			fixed4 _position;

			fixed4 BorderEdge(fixed2 centre, fixed4 size)
			{
				fixed4 edge = 0.0;
				edge.xy = centre - size.xy;
				edge.zw = 1.0 - (centre + size.zw);

				return edge;
			}

			fixed4 rectangle(fixed4 col, fixed2 coord, fixed2 centre, fixed4 size, fixed4 blurr)
			{
				fixed4 borders = 1.0;

				fixed4 borderEdge = BorderEdge(centre, size);

				// smoothstep(edge0, edge1, x) // returs value between 0 and 1
				borders.xy = smoothstep(borderEdge.xy, borderEdge.xy + blurr, coord);
				borders.zw = smoothstep(borderEdge.zw, borderEdge.zw + blurr, 1.0 - coord);

				col.rgb *= borders.x * borders.y * borders.z * borders.w;

				return col;
			}

			fixed4 outlineRectangle(fixed4 col, fixed2 coord, fixed2 centre, fixed4 size, fixed4 bSize)
			{
				fixed4 outlineBorder = 1.0;
				fixed4 withinRect = 1.0;

				fixed4 EdgeInside = 1.0 - BorderEdge(centre, size);
				fixed4 EdgeOutside = BorderEdge(centre, size + bSize);
				
				// Rectangle bounderies
				withinRect.xy = step(EdgeOutside.xy, coord);
				withinRect.zw = step(EdgeOutside.zw, 1.0 - coord);
				
				fixed leftRight = withinRect.x * withinRect.z;
				fixed bottomTop = withinRect.y * withinRect.w;
				
				// Outline
				outlineBorder.xy = step(EdgeOutside.xy, coord) * step(EdgeInside.xy, 1.0 - coord); // Left Bottom
				outlineBorder.zw = step(EdgeInside.zw, coord) * step(EdgeOutside.zw, 1.0 - coord); // Right Top

				// Make sure it's within the rectangle bounderies
				outlineBorder.xz *= bottomTop;
				outlineBorder.yw *= leftRight;

				// max can be used as an OR logical operator ( min(a+b, 1.0) alternative for values not locked in 0, 1 range)
				col.rgb *= 1.0 - max(max(outlineBorder.x, outlineBorder.z), max(outlineBorder.y, outlineBorder.w));

				return col;
			}

			fixed4 floorRectangle(fixed4 col, fixed2 coord)
			{
				fixed4 borders = 1.0;
				borders.xy = floor(coord.xy + .9);
				borders.zw = floor(1.0 - coord.xy + .9);
				//borders = floor(coord.xyxy + fixed4(.8));

				col.rgb *= borders.x * borders.y * borders.z * borders.w; 

				return col;
			}

			// https://docs.unity3d.com/455/Documentation/Manual/SL-BuiltinValues.html
			// 
			//vec2 midpoint = screenDim_.xy * 0.5;
			//float radius = min(screenDim_.x, screenDim_.y) * circleRadius_;
			//float dist = length(gl_FragCoord.xy - midpoint);
			//circle = smoothstep(radius-1.0, radius+1.0, dist)
			//
			fixed4 circle(fixed4 col, fixed2 coord, fixed2 position, fixed2 radii)
			{
				fixed2 vhalf = fixed2(.5, .5);
				fixed4 black = fixed4(0.0, 0.0, 0.0, 1.0);

				fixed2 ratio = fixed2(1.0, 1.0 / (_ScreenParams.x / _ScreenParams.y)); // 4:3
				fixed dist = length((coord - position) * ratio);

				// W < H ratio == min
				// W > H ratio == max
				fixed greaterHeight = step(_ScreenParams.x, _ScreenParams.y);
				fixed greaterWidth = step(_ScreenParams.y, _ScreenParams.x);
				fixed equalSize = greaterWidth * greaterHeight;

				// min is for narrow and tall
				// max is for wide and short
				// unless W == H then we just use the min
				fixed mod = min(_ScreenParams.x, _ScreenParams.y) * greaterHeight; 			
				mod += max(_ScreenParams.x, _ScreenParams.y) * (greaterWidth - equalSize); 	

				fixed outerRadius = radii.x / mod;
				fixed innerRadius = radii.y / mod;

				fixed sm = smoothstep(outerRadius, innerRadius, dist);
				col.rgb *= sm;

				//col.rgb = lerp(col.rgb, black.rgb, smm);

				return col;
			}

			fixed4 posRadii;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 size = fixed4(300, 300, 300, 300);
				fixed4 borderSize = 20;
				fixed2 coord = i.uv;
				fixed4 col = tex2D(_MainTex, coord);

				fixed4 nSize = size / _ScreenParams.xyxy;
				fixed4 bSize = borderSize / _ScreenParams.xyxy;

				fixed2 centre = fixed2(sin(_Time.x + 0.75), 0.5 + 0.5 * _SinTime.w); //0.5 + 0.25 * _SinTime.w
				centre = fixed2(0.5 + 0.5 * _SinTime.w, .5);

				fixed2 position = posRadii.xy / _ScreenParams.xy;

				//col = rectangle(col, coord, centre, nSize, 0.0001);
				//col = outlineRectangle(col, coord, centre, nSize, bSize);
				col = circle(col, coord, position, posRadii.zw);

				return col;
			}

			ENDCG
		}
	}
}
