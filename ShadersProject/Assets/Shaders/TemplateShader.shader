﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TemplateShader" 
{
	// Language: Cg / HLSL
	// functions
	// dot(v1, v2) - dot product of 2 vectors; range: [-1, 1]
	// ComputeGrabScreenPos(v1) - 
	// step(edge, x) - function by comparing x to edge; returns 0.0 if x < edge else 1 (use this to remove if statements)
	// smoothstep(edge0, edge1, x) // returs value between 0 and 1; edge0 < x < edge1; percentage x between edges

	// Unity specific functions:
	// WorldNormalVector - Map normals to world coordinates
	// UNITY_PROJ_COORD(v1) - 
	// mul(UNITY_MATRIX_MVP, v1) - Convert the vertex position to screen position
	//
	// Unity variable
	// _Time
	// _SinTime
	//
	// Unity supp materials
	// Matte	- Used for opaque surfaces
	// Specular - Used for surfaces which has reflections
	// Unity 4.x default diffuse lightning model - Lambertian reflectance
	// for specular reflections the Blinn-Phong technique is used

	// Unity 5.x Physically Based Rendering
	// Physically based Standard lighting model, and enable shadows on all light types
	//#pragma surface surf Standard fullforwardshadows // <-- Use PBR Lightning
	// Use shader model 3.0 target, to get nicer looking lighting
	//#pragma target 3.0 // <-- Advanced features enabled (won't work on outdated hardware)
	// SurfaceOutputStandard must be used for PBR
	// void surf (Input IN, inout SurfaceOutputStandard o) {}
	//
	// Shading Technique used in Unity
	// Indicating in order: shading technique, surface shader name, surface output structure name 
	// and name of the respective built-in shader
	//
	//			|	Unity4 and below			|	Unity 5 and above
	//----------|-------------------------------------------------------------------------------------
	// Diffuse	|	Lambertian reflectance		|	Physically Based Rendering (Metallic)
	//			|	Lambert, SurfaceOutput		|	Standard, SurfaceOutputStandard
	//			|	Bumbed Diffuse				|	Standard
	//----------|-------------------------------|----------------------------------------------------
	// Specular	|	Blinn-Phong reflection		|	Physically Based Rendering (Specular)
	//			|	BlinnPhong, SurfaceOutput	|	StandardSpecular, SurfaceOutputStandardSpecular
	//			|	Bumbed SPecular				|	Standard (Specular setup)
	//
	//
	// Binding Semantics
	// When a colon is placed after a variable or a function, it is used to indicates that the variable itself will play a special role.
	// Examples:
	// float4 pos : POSITION 	// Indicates pos initialised with vertex position
	// float4 pos : SV_POSITION	// Indicates pos initialised with screen position of a vertex
	//
	// Input semantics vertInput fields:
	// POSITION, SV_POSITION: the position of a vertex in world coordinates (object space);
	// NORMAL: the normal of a vertex, relative to the world (not to the camera);
	// COLOR, COLOR0, DIFFUSE, SV_TARGET: the color information stored in the vertex;
	// COLOR1, SPECULAR: the secondary color information stored in the vertex;
	// FOGCOORD: the fog coordinate;
	// TEXCOORD0, TEXCOORD1, …, TEXCOORDi: the i-th UV data stored in the vertex
	//
	// Output semantics vertOutput fields:
	// POSITION, SV_POSITION, HPOS: the position of a vertex in camera coordinates (clip space, from zero to one for each dimension);
	// COLOR, COLOR0, COL0, COL, SV_TARGET: the front primary colour;
	// COLOR1, COL1: the front secondary colour;
	// FOGC, FOG: the fog coordinate;
	// TEXCOORD0, TEXCOORD1, …, TEXCOORDi, TEXi: the i-th UV data stored in the vertex;
	// PSIZE, PSIZ: the size of the point we are drawing;
	// WPOS: the position, in pixel, within the window (origin in the lower left corner)
	//
	// Distortion - Bump map - usually used to indicate how light shoeld reflect onto a surface
	//
	// Camera effects:
	// Attach C#/js script to the camera
	// Add function void OnRenderImage(RenderTexture source, RenderTexture destination) {}
	// Call Graphics.Blit(source, destination, material)
	// Create a Image effects file

	Properties
    {
        // The properties of your shaders
        // - textures
        // - colours
        // - parameters
        // ...

        // Properties equivalent to public fields in C# scripts (appear in Inspector)
        // Properties need to be defined  within the SubShader scope
        // Materials are assets; 
        // Changes made to the properties of the material while running the game in the editor are permanent;
        _MyTexture ("My texture", 2D) = "white" {}
    	_MyNormalMap ("My normal map", 2D) = "bump" {}  // Grey

	 	_MyInt ("My integer", Int) = 2
		_MyFloat ("My float", Float) = 1.5
    	_MyRange ("My range", Range(0.0, 1.0)) = 0.5

    	_MyColor ("My colour", Color) = (1, 0, 0, 1)    // (R, G, B, A)
    	_MyVector ("My Vector4", Vector) = (0, 0, 0, 0) // (x, y, z, w)
    }

    SubShader
    {
    	// Tags are a way of telling Unity certain properties of the shader we are writing
    	// Telling the Order which it should be rendered (Queue) (Draw Order: +int. From (start)Low int to (end)High int)
    	// Telling how it should be rendered (RenderType)
    	Tags
    	{
    		"Queue" = "Geometry" // Background(1000) | Geometry (2000)  | Transparent (3000) | OverLay (4000)
        	"RenderType" = "Opaque"
    	}

    	// Grabpass is used when the shaders becomes more complicated that it needs to be rendered multiple times in different passes
    	// GrabPass captures what has already been drawn onto the textures and re-output it onto the screen
    	//GrabPass { "_GrabTexture"}
    	// sampler2D _GrabTexture - DON'T forget to declare a variable for it

    	Pass
    	{
    		// ZTest which stops hidden pixels from being drawn
    		// Pixels contains the depth (distance from the camera) of the object draw in that pixel
    		// No culling or depth
			//Cull Off ZWrite Off ZTest Always

    		CGPROGRAM
    		// Cg / HLSL code of the shader
        	// - surface shader
        	//    OR
        	// - vertex and fragment shader
        	//    OR
        	// - fixed function shader

        	//#include "UnityCG.cginc"

        	// Defining the properties inside the SubShader scope
        	// Names of the parameters must match the ones of the properties scope
		 	sampler2D _MyTexture;
			sampler2D _MyNormalMap;
			 	
    		int _MyInt;
    		float _MyFloat;
    		float _MyRange;
    		half4 _MyColor;
    		float4 _MyVector;

    		// Datatypes
    		// float 	- 32 bit precision
    		// half 	- 16 bit precision
    		// fixed 	- 10 bit precision and value spans between -2 and 2
    		// floatN, halfN, fixedN - replace N with 2, 3, 4 to create vectors (packed arrays)
    		// Access to subfield members - Can be in any order | Select any subfield you need
    		// float.xyzw | float.rgba

    		// Sampling textures
    		// Textured models made up of several triangles, each one made of three vertices
    		// Vertices are used to store UV and color data
    		// UV is 2D vector which indicates which point of the texture is mapped to the vertex of that model

    		#pragma vertex vert             
    	    #pragma fragment frag
		    
    	    struct vertInput {
    	        float4 pos : POSITION;
    	    };  
		    
    	    struct vertOutput {
    	        float4 pos : SV_POSITION;
    	    };
		    
    	    vertOutput vert(vertInput input) {
    	        vertOutput o;
    	        o.pos = UnityObjectToClipPos(input.pos);
    	        return o;
    	    }
		    
    	    half4 frag(vertOutput output) : COLOR {
    	        return half4(1.0, 0.0, 0.0, 1.0); 
    	    }

    		// ...
    		ENDCG
    	}

		// Surface Shader
		// Used when lights needs to affect the material in a realistic way (specify physical properties of the materials)
		// Surface shaders hide the calculations of how light is reflected and allows to specify custom light properties
		// Surface shaders do not have to be enclosed within a Pass Section
		// Albedo of material = base colour
		// Before sending the vertices to the surface function you can pass them through the Vertex function
		// Surf funcion manipulates the colours in the RGBA space
		// Vertex function allows you to manipulate the vertices of a model
		// Cel shading

		// SurfaceInput properties
		// float3 viewDir: 			the direction of the camera (view direction);
		// float4 name : COLOR: 	by using this syntax, the variable name will contain the colour of the vertex;
		// float4 screenPos: 		the position on the current pixel on the screen;
		// float3 worldPos: 		the position of the current pixel, in world coordinates.

		// SurfaceOutput properties:
		// fixed3 Albedo: 	the base colour / texture of an object,
		// fixed3 Normal: 	the direction of the face, which determines its reflection angle,
		// fixed3 Emission: how much light this object is generating by itself,
		// half Specular: 	how well the material reflects lights from 0 to 1,
		// fixed Gloss: 	how diffuse the specular reflection is,
		// fixed Alpha: 	how transparent the material is.

		// SurfaceOutputStandard properties:
		// fixed3 Albedo:			See Above
		// fixed3 Normal:			See Above
		// fixed3 Emission::		See Above
		// fixed  Alpha:			See Above
		// half Metallic:			How Metallic the object is; Value range [0, 1]; Determines how light reflects on the material
		// half Smoothness:			Indicates how smooth the surface is; Range [0, 1]
		// half Occlusion: 			Indicates the amount of ambient occlusion

		// SurfaceOutputStandardSpecular properties:
		// replace Metallic with float3 Specular;

        // TEMPLATE SURFACE SHADER
		//CGPROGRAM
		// Line 124 specifies surface function for this shader is 'surf'. It also tells what kind of model should be used
		//// Uses the Lambertian lighting model
		//#pragma surface surf Lambert vertex:vert
		//
		//sampler2D _MainTex; // The input texture
		//
		//struct Input {
		//    float2 uv_MainTex;
		//};
		// Example function inflates model
		//void vert(inout appdata_full v) {
		//	v.vertex.xyz += v.normal * 2;
		//}
		//
		//void surf (Input IN, inout SurfaceOutput o) {
		// Map the texture onto the model
		//    o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
		//}
		//ENDCG
		//

		// Vertex and fragment Shader
		// No built-in concept of how lights should behave.
		// Geometry of your model is FIRST passed through function called vert which can alter its vertices. (Alter the position and data of each vertex)
		// Afterwards individual triangles are passed through the function called frag which decides the final RGB colour of every pixel.
		// Usefull for 2D effects, post proceessing and special 3D effects which are too complex to be expressed as Surface Shader
		// Vertex and Fragment shaders needs to be enclosed within a Pass Section
		// TEMPLATE VERTEX AND FRAGMENT SHADER
		//Pass
		//{
		    //CGPROGRAM
		    //
		    // 102 & 103 specifing the vert and frag functions for this shader
    	    //#pragma vertex vert             
    	    //#pragma fragment frag
		    //
    	    //struct vertInput {
    	    //    float4 pos : POSITION;
    	    //};  
		    //
    	    //struct vertOutput {
    	    //    float4 pos : SV_POSITION;
    	    //};
		    //
    	    //vertOutput vert(vertInput input) {
    	    //    vertOutput o;
    	    //    o.pos = mul(UNITY_MATRIX_MVP, input.pos);
    	    //    return o;
    	    //}
		    //
    	    //half4 frag(vertOutput output) : COLOR {
    	    //    return half4(1.0, 0.0, 0.0, 1.0); 
    	    //}
    	    //ENDCG
		//}
    }   
}
