﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RGBCameraEffect : MonoBehaviour {
	[ColorUsage(true, true, 0, 1, 0.125f, 1)]
	public Color colMult = new Color(1.0f, 1.0f, 1.0f, 1.0f);

	private Material mat;

	void Awake()
	{
		mat = new Material(Shader.Find("Hidden/RGBFilter"));
	}

	void Update()
	{
		mat.SetColor("_ColMult", colMult);
	}

	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		Graphics.Blit(src, dest, mat);
	}
}
