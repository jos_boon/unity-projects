﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(AudioSource))]
public class SpectrumToShader : MonoBehaviour {

	Material mat;
	AudioSource aSource;
	StreamWriter sw;

	// Use this for initialization
	void Start () 
	{
		mat = GetComponent<SpriteRenderer>().material;
		aSource = GetComponent<AudioSource>();
		sw = CreateFile();
		sw.Close();
	}

	// Update is called once per frame
	void Update () 
	{
		
		//DrawSpectrum();
		DrawData();


		/*if(sw != null && i == 50)
		{
			for (int i = 0; i < data.Length; i++)
			{
				sw.WriteLine(data[i].ToString());
			}

			sw.Close();
			sw = null;
		}
*/

	}

	void DrawSpectrum()
	{
		float[] spectrum = new float[256];
		aSource.GetSpectrumData(spectrum, 0, FFTWindow.Rectangular);
		mat.SetFloatArray("spectrum", spectrum);

		for (int i = 1; i < spectrum.Length - 1; i++)
		{
			//Debug.DrawLine(new Vector3(i - 1, spectrum[i] + 10, 0), new Vector3(i, spectrum[i + 1] + 10, 0), Color.red);
			//Debug.DrawLine(new Vector3(Mathf.Log(i - 1), spectrum[i - 1] - 10, 1), new Vector3(Mathf.Log(i), spectrum[i] - 10, 1), Color.green);

			Debug.DrawLine(new Vector3(i - 1, Mathf.Log(spectrum[i - 1]) + 10, 2), new Vector3(i, Mathf.Log(spectrum[i]) + 10, 2), Color.cyan);
			Debug.DrawLine(new Vector3(Mathf.Log(i - 1), Mathf.Log(spectrum[i - 1]), 3), new Vector3(Mathf.Log(i), Mathf.Log(spectrum[i]), 3), Color.blue);

		}
	}

	void DrawData()
	{
		float[] data = new float[256];
		aSource.GetOutputData(data, 0);
		int height = 100;
		for (int i = 1; i < data.Length; i++)
		{
			//texture.SetPixel(width * i / size, height * (samples[i]+1f)/2, waveformColor);
			Debug.DrawLine(new Vector3(i-1, data[i-1] * height, 2), new Vector3(i, height * data[i], 2), Color.red);
		}

	}

	void DrawWaveForm(AudioClip clip)
	{
		
	}



	StreamWriter CreateFile()
	{
		int i = 0;

		while(File.Exists("dump/spectrum" + i.ToString("00") + ".txt"))
			i++;

		return new StreamWriter(File.Create("dump/spectrum" + i.ToString("00") + ".txt"));
	}

}
