﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyCameraEffect : MonoBehaviour {

	// Change booleans to private
	public bool Trigger = false;
	public bool revTrigger = false; // reverse Trigger
	public bool greyState = false;
	public float speed = 1.0f;

	public Transform tracker;
	public Vector2 radii = new Vector2(300, 100);
	public float spotlightSpeed = 5.0f;
	private Vector2 prevScreenPosition;

	public Shader[] shaders;

	private Material[] mats;
	private int currMat = 0;
	private float step = 0.0f;

	private Camera c;

	void Awake()
	{
		c = Camera.main;

		// This could crash if shaders has no shaders in it
		mats = new Material[shaders.Length];
		for(int i = 0; i < shaders.Length; i++)
			mats[i] = new Material(shaders[i]);

		tracker = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void Update()
	{
		keyboardControls();

		if(currMat == 0)
			HandleGreyScale();
		// currMat == 1
		// 	MirrorEffect
		else if(currMat == 2)
			HandleSpotLight();
	}

	void HandleSpotLight()
	{
		Vector3 position = tracker != null ? tracker.position : Vector3.zero;
		Vector2 screenPostion = c.WorldToScreenPoint(position);
		prevScreenPosition = Vector2.Lerp(prevScreenPosition, screenPostion, Time.deltaTime * spotlightSpeed);

		Vector4 posRadii = new Vector4(prevScreenPosition.x, prevScreenPosition.y, radii.x, radii.y);

		mats[currMat].SetVector("posRadii", posRadii);
	}

	void HandleGreyScale()
	{
		if(Trigger)
		{
			step += Time.deltaTime * speed;
			Trigger = !(step >= 1.0f);

			mats[currMat].SetFloat("_Step", step);
		}

		if(revTrigger)
		{
			step -= Time.deltaTime * speed;
			revTrigger = !(step <= 0.0f);

			mats[currMat].SetFloat("_Step", step);
		}
	}

	private void keyboardControls()
	{
		if(Input.GetKeyDown(KeyCode.Space))
			Toggle();
		else if(Input.GetKeyDown(KeyCode.Tab))
			NextMaterial();
	}

	public void Toggle()
	{
		if(currMat == 0)
		{
			greyState = !greyState;
			Trigger = greyState;
			revTrigger = !greyState;
		}
	}

	public void NextMaterial()
	{
		currMat = ++currMat % mats.Length;
	}
		
	void OnRenderImage(RenderTexture src, RenderTexture dest)
	{
		if(mats.Length > 0)
			Graphics.Blit(src, dest, mats[currMat]);
		else
			Graphics.Blit(src, dest);
	}

}
