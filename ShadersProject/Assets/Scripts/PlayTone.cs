﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayTone : MonoBehaviour {

	AudioClip soundfx;
	AudioSource aSource;

	public int position = 0;
	public int samplerate = 44100; // Samples per second
	public int frequency = 440;

	// Use this for initialization
	void Start () {
		aSource = GetComponent<AudioSource>();
		//AudioClip.Create("CustomTone", Samples per second, Number of mics (mono, stereo, surround),  samplerate, true, OnAudioRead, OnAudioSetPosition);
		soundfx = AudioClip.Create("CustomTone", samplerate * 2, 1, samplerate, true, OnAudioRead, OnAudioSetPosition);
		aSource.clip = soundfx;

		aSource.Play();
	}

	void OnAudioRead(float[] data)
	{
		int count = 0;
		while (count < data.Length)
		{
			data[count] = Mathf.Sign(Mathf.Sin(2 * Mathf.PI * frequency * position / samplerate));
			position++;
			count++;
		}
	}

	void OnAudioSetPosition(int newPosition)
	{
		position = newPosition;
	}
}
