﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class MoveBehaviour : MonoBehaviour {

	public float force = 100;

	Rigidbody2D rb2d;
	// Use this for initialization
	void Awake()
	{
		rb2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");

		rb2d.AddForce(new Vector2(x, y) * force * Time.fixedDeltaTime);
	}
}
