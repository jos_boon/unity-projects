﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyTexture : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SpriteRenderer sRender = GetComponent<SpriteRenderer>();
		Sprite s = sRender.sprite;
		//sRender.material.SetTexture("Texture", s.texture);

		//printComponents();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void printComponents()
	{
		var list = GetComponents<Component>();

		foreach(var item in list)
			Debug.Log(item.GetType().ToString());
		
	}

}
