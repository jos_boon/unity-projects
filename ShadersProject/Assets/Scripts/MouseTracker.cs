﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseTracker : MonoBehaviour {

	public float SecondsPerHit = 0.15f;
	public float speed = 8.0f;
	public Vector4 currPointOnSprite = new Vector4(0,0,0,0);
	public Vector4 prevPointOnSprite = new Vector4(0,0,0,0);

	private Material mat;
	private Camera mainCam;

	/// <summary>
	/// Sending value to shader won't loose it's data
	/// But it needs to be resent when value is changed
	/// </summary>

	// Use this for initialization
	void Start () {
		mainCam = Camera.main;
		mat = GetComponent<SpriteRenderer>().material;
		mat.SetVector("_point", currPointOnSprite);
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void OnMouseOver()
	{
		currPointOnSprite = Input.mousePosition;
		prevPointOnSprite = Vector3.Lerp(prevPointOnSprite, currPointOnSprite, Time.deltaTime * speed);

		mat.SetVector("_point", getPixelPosition(prevPointOnSprite));
	}

	// Not actual pixel position (unity units)
	Vector4 getPixelPosition(Vector3 position)
	{
		position.z = Mathf.Abs(mainCam.transform.position.z - transform.position.z); // Distance away from the camera
		Vector3 worlPosition = mainCam.ScreenToWorldPoint(position);
		Vector3 localPosition = transform.InverseTransformPoint(worlPosition); // value xy range [-0.5, 0.5]

		return localPosition + new Vector3(.5f, .5f, 0); // Shift position within range [0.0, 1.0]
	}


}
