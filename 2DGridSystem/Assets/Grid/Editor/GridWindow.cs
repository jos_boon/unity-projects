﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GridWindow : EditorWindow
{
	bool snap = true;
	bool showGrid = false;
	string gridType = "Hexagon";
	GameObject source;
	Vector3 halfSize = Vector3.zero;
	//IGridBehaviour grid = new HexagonGridBehaviour();
	IGridBehaviour grid = new SquareGridBehaviour();

	public GameObject Source 
	{
		get
		{
			return source;
		}
		private set
		{
			// TODO: I'm assuming that the tile that is beining picked has a SpriteRenderer
			source = value;
			if(source != null)
			{
				halfSize = source.GetComponent<SpriteRenderer>().sprite.bounds.extents;
				Debug.Log(halfSize.ToString());
			}
		}
	}

	[MenuItem ("Window/Grid Window")]
	public static void ShowWindow()
	{
		EditorWindow.GetWindow(typeof(GridWindow));
	}

	void OnGUI()
	{
		GUILayout.Label("Settings", EditorStyles.boldLabel);
		gridType = EditorGUILayout.TextField("Type Grid", gridType);
		snap = EditorGUILayout.Toggle("Snap To Grid", snap);
		showGrid = EditorGUILayout.Toggle("Show Grid", showGrid);
		Source = (GameObject)EditorGUILayout.ObjectField("obj", source, typeof(GameObject), false);
	}

	void OnFocus()
	{
		// Remove old delegate
		SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
		// (re-)Add delegate
		SceneView.onSceneGUIDelegate += this.OnSceneGUI;
	}

	void OnDestroy()
	{
		// Clean up
		SceneView.onSceneGUIDelegate -= this.OnSceneGUI;
	}

	void OnSceneGUI(SceneView sceneView)
	{
		Event e = Event.current;
		if(e.isMouse)
		{
			Vector2 mousePos = e.mousePosition;

			if(e.button == 0 && e.type == EventType.MouseDown)// e.isMouse)
			{
				if(source != null)
				{
					// Camera we use to navigate in the scene
					Camera c = sceneView.camera;
					// Translate the Mouse point to a Screen Point
					Vector2 screen = HandleUtility.GUIPointToScreenPixelCoordinate(e.mousePosition);

					// Translate the Screen Point to a World Point
					Vector3 worldPoint = c.ScreenToWorldPoint(new Vector3(screen.x, screen.y, sceneView.cameraDistance));

					// Restrict placing object to a grid format
					Vector3 gridPosition = grid.DoBehaviour(worldPoint, new Vector2(1, 1), 0.0f);

					// Create Object and place within world space
					GameObject clone = SceneView.Instantiate<GameObject>(Source, gridPosition, Quaternion.identity);
					//clone.name = "HexaTile";
				}
				else
				{
					Debug.Log("No Tile selected");
				}
			}
				
		}
	}
}

public interface IGridBehaviour
{
	Vector3 DoBehaviour(Vector3 worldPosition, Vector2 gridSize, float zLayer);
}

public class SquareGridBehaviour : IGridBehaviour
{
	public Vector3 DoBehaviour(Vector3 worldPosition, Vector2 gridSize, float zLayer)
	{
		Vector3 gridPosition = Vector3.zero;
		Vector3 halfsize = gridSize / 2.0f;
		Vector2 direction = new Vector2(1, 1);
		direction.x = worldPosition.x > 0 ? 1 : -1;
		direction.y = worldPosition.y > 0 ? 1 : -1;

		gridPosition.x = worldPosition.x - worldPosition.x % gridSize.x + halfsize.x * direction.x;
		gridPosition.y = worldPosition.y - worldPosition.y % gridSize.y + halfsize.y * direction.y;
		gridPosition.z = zLayer;

		return gridPosition;
	}
}

public class HexagonGridBehaviour : IGridBehaviour
{
	public Vector3 DoBehaviour(Vector3 worldPosition, Vector2 gridSize, float zLayer)
	{
		Vector3 gridPosition = Vector3.zero;
		Vector3 halfsize = gridSize / 2.0f;
		Vector2 direction = new Vector2(1, 1);
		direction.x = worldPosition.x > 0 ? 1 : -1;
		direction.y = worldPosition.y > 0 ? 1 : -1;

		gridPosition.y = worldPosition.y - worldPosition.y % gridSize.y + halfsize.y * direction.y;
		gridPosition.x = worldPosition.x - worldPosition.x % gridSize.x + halfsize.x * direction.x;

		if(worldPosition.y - worldPosition.y % 2 == 0)
			gridPosition.x += halfsize.x * direction.x;
		
		gridPosition.z = zLayer;

		return gridPosition;
	}
}
