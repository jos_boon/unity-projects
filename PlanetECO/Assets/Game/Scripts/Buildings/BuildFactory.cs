﻿using UnityEngine;
using System.Collections;

public class BuildFactory : MonoBehaviour 
{
    public int costRefinery;
    public int powerConsumedByRefinery;
    public int harvestPerSecondRefinery;

    public int costPowerPlant;
    public int powerConsumedByPowerPlant;
    public int powerGeneratedByPowerPlant;

    private static BuildFactory instance;

    public static BuildFactory Instance
    {
        get 
        {
            if (instance == null)
                instance = FindObjectOfType<BuildFactory>();


            return instance; 
        }
    }

    void Awake()
    {
        if (Instance != this)
            Destroy(gameObject);
    }

    public Building getRefinery(PlanetResources p, ResourceManager resource)
    {
        return new Refinery(p, resource, costRefinery, powerConsumedByRefinery, harvestPerSecondRefinery);
    }

    public Building getPowerPlant(PlanetResources p, ResourceManager resource)
    {
        return new PowerPlant(p, resource, costPowerPlant, powerConsumedByPowerPlant, powerGeneratedByPowerPlant);
    }


}
