﻿using UnityEngine;
using System.Collections;

abstract public class Building
{
	public int BuildingMaterialCost;
	public int consumePower;
	protected PlanetResources planetRes;
	protected ResourceManager resMan;
	private bool isPaidFor = false;

    protected bool hasPower
    {
        get { return resMan.AvaiblePower() >= 0; }
    }

	abstract public void doBehaviour();

	virtual public bool buyBuilding(out string message)
	{
		message = "";

		if(isPaidFor)
		{
			message = "This building has already been paid for";
			return false;
		}

		if(resMan.TotalBuildingMaterials >= BuildingMaterialCost)
		{
			if(resMan.AvaiblePower() >= consumePower)
				message = "You need more power!";

            resMan.TotalBuildingMaterials -= BuildingMaterialCost;
			resMan.PowerInUse += consumePower;
			isPaidFor = true;
			return true;
		}

		message = "Not enough building materials!";
		return false;
	}

	virtual public void sellBuilding()
	{
		resMan.TotalBuildingMaterials += BuildingMaterialCost / 2;
		resMan.PowerInUse -= consumePower;
	}

}
