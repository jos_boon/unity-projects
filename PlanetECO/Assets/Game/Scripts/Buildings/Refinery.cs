﻿using UnityEngine;
using System.Collections;

public class Refinery : Building 
{
	public int harvestPerSecond;

	public Refinery(PlanetResources planRes, ResourceManager resMan, int BuildingMaterialCost, int consumePower, int harvestPerSecond)
	{
		this.consumePower = consumePower;
		this.BuildingMaterialCost = BuildingMaterialCost;
		this.planetRes = planRes;
		this.harvestPerSecond = harvestPerSecond;
		this.resMan = resMan;
	}

	// Harvest Materials of the planet
	public override void doBehaviour ()
	{
		if(!hasPower)
			return;

		if(planetRes.BuildingMaterials >= harvestPerSecond)
		{
			planetRes.BuildingMaterials -= harvestPerSecond;
			resMan.TotalBuildingMaterials += harvestPerSecond;
		}
		else
		{
			resMan.TotalBuildingMaterials += planetRes.BuildingMaterials;
			planetRes.BuildingMaterials -= 0;
		}
	}

}
