﻿using UnityEngine;
using System.Collections;

public class PowerPlant : Building 
{
	public int GeneratePower;
	public bool hasGenerated;

	public PowerPlant(PlanetResources planRes, ResourceManager resMan, int BuildingMaterialCost, int consumePower, int GeneratePower)
	{
		this.planetRes = planRes;
		this.resMan = resMan;
		this.BuildingMaterialCost = BuildingMaterialCost;
		this.consumePower = consumePower;
		this.GeneratePower = GeneratePower;
	}

	public override void doBehaviour ()
	{
		if(!hasGenerated)
		{
			planetRes.Energy -= GeneratePower;
			resMan.totalPower += GeneratePower;
			hasGenerated = true;
		}
	}

	public override void sellBuilding ()
	{
		base.sellBuilding();

		planetRes.Energy += GeneratePower;
		resMan.totalPower -= GeneratePower;
	}
}
