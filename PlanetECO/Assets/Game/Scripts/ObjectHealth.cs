﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectHealth : MonoBehaviour 
{
	public int maxHitPoints = 1;
	public Slider healthSlider;

	private int hitPoints;
	public int HitPoints
	{
		get { return hitPoints; }
		set
		{
			hitPoints = value;

			// Update slider
			// Play hit audio

			if(hitPoints > maxHitPoints)
				hitPoints = maxHitPoints;
			else if(hitPoints <= 0)
				print ("Object Destroyed!");
		}
	}

	void Awake()
	{
		hitPoints = maxHitPoints;
	}

	void Destroyed()
	{
		// Disable other components (shooting and moving)
		// Play Destroy sound
	}

}
