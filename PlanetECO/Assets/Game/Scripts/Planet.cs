﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Planet : MonoBehaviour 
{
	public int maxBuildings = 1;
	public float delay = 1;

	private List<Building> buildings;
	private int nrOfRefinery = 0;
	private int nrOfPowerPlants = 0;

	public int NrOfRefinery
	{
		get { return nrOfRefinery; }
	}

	public int NrOfPowerPlants
	{
		get { return nrOfPowerPlants; }
	}
	
	void Start () 
	{
		buildings = new List<Building>();
		InvokeRepeating("UpdateBuildings", delay, delay);
	}

	private void UpdateBuildings()
	{
		for (int i = 0; i < buildings.Count; i++) 
		{
			buildings[i].doBehaviour();
		}
	}

	public void addBuilding(Building b)
	{
		if(buildings.Count < maxBuildings)
		{
            string message;
            if (b.buyBuilding(out message))
            {
                buildings.Add(b);
                counter(b.GetType(), 1);
            }
            else
                print(message);
			
		}
	}

	public void removeBuilding(Type t)
	{
        Building b = buildings.Find(x => x.GetType() == t);

        if (b == null)
            return;

        b.sellBuilding();

		buildings.Remove(b);
		counter(b.GetType(), -1);
	}

	public int nrOfBuildings()
	{
		return buildings.Count;
	}

	private void counter(Type t, int val)
	{
		if(t == typeof(Refinery))
			nrOfRefinery += val;
		else if(t == typeof(PowerPlant))
			nrOfPowerPlants += val;
	}
}
