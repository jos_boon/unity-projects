﻿using UnityEngine;
using System.Collections;

public class ResourceManager : MonoBehaviour 
{
	public int totalBuildingMaterials;
	public int totalPower;

	private int powerInUse;

	public int TotalBuildingMaterials
	{
		get { return totalBuildingMaterials; }
		set
		{
			totalBuildingMaterials = value;

			if(totalBuildingMaterials <= 0)
			{
				totalBuildingMaterials = 0;
				print ("Out of materials!");
			}
		}
	}

	public int PowerInUse
	{
		get { return powerInUse; }
		set
		{
			powerInUse = value;

			if(powerInUse > totalPower)
				print ("Power sortage!");
		}
	}

	public int AvaiblePower()
	{
		return totalPower - powerInUse;
	}
}
