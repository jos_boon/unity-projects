﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlanetUIUpdater : MonoBehaviour 
{
	public Planet planet { get; private set; }
	private PlanetResources planetRes;
    private ResourceManager baseResource;
	
	private Text txtMaterial;
	private Text txtEnergy;
	private Text txtRefinery;
	private Text txtPowerPlant;
	private Text txtTotalBuildings;

	void Awake()
	{
		txtMaterial = GameObject.Find("txtMaterials").GetComponent<Text>();
		txtEnergy = GameObject.Find("txtEnergy").GetComponent<Text>();
		txtRefinery = GameObject.Find("txtRefinery").GetComponent<Text>();
		txtPowerPlant = GameObject.Find("txtPowerPlant").GetComponent<Text>();
		txtTotalBuildings = GameObject.Find("txtTotalBuildings").GetComponent<Text>();

        baseResource = GameObject.FindGameObjectWithTag("Player").GetComponent<ResourceManager>();
	}

    void LateUpdate()
    {
        UpdateTextFields();
    }

	public void Initialize(Planet p, PlanetResources res)
	{
		planet = p;
		planetRes = res;

		UpdateTextFields();
	}

	void UpdateTextFields()
	{
		if(!planet || !planetRes)
			return;

		txtMaterial.text = "Remainding Materials: " + planetRes.BuildingMaterials.ToString();
		txtEnergy.text = "Energy: " + planetRes.Energy.ToString();
		txtRefinery.text = "Refinery: " + planet.NrOfRefinery;
		txtPowerPlant.text = "Power Plants: " + planet.NrOfPowerPlants;
		txtTotalBuildings.text = planet.nrOfBuildings().ToString() + " / " + planet.maxBuildings.ToString();
	}

    public void BuyRefinery()
    {
        planet.addBuilding(BuildFactory.Instance.getRefinery(planetRes, baseResource));
    }

    public void SellRefinery()
    {
        planet.removeBuilding(typeof(Refinery));
    }

    public void BuyPowerPlant()
    {
        planet.addBuilding(BuildFactory.Instance.getPowerPlant(planetRes, baseResource));
    }

    public void SellPowerPlant()
    {
        planet.removeBuilding(typeof(PowerPlant));
    }
}
