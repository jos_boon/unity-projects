﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlanetUIBehaviour : MonoBehaviour 
{
	public Planet planet;
	public PlanetResources planetRes;
	public PlanetUIUpdater planetUI;
	public Vector3 offset = Vector3.zero;
	private static bool openPlanetUI = false;

	public bool OpenPlanetUI
	{
		get { return openPlanetUI; }
		set
		{
			openPlanetUI = value;

			if(openPlanetUI)
			{
				planetUI.gameObject.SetActive(true);
				planetUI.transform.position = transform.position + offset;
				planetUI.Initialize(planet, planetRes);
			}
			else
				planetUI.gameObject.SetActive(false);
		}
	}

	void Awake()
	{
		planetUI = GameObject.Find("PlanetUI").GetComponent<PlanetUIUpdater>();
	}

	void Start () 
	{
		if(!planet)
			planet = GetComponent<Planet>();

		if(!planetRes)
			planetRes = GetComponent<PlanetResources>();

		if(planetUI.gameObject.activeSelf)
			planetUI.gameObject.SetActive(false);
	}

	void OnMouseOver()
	{
		if(Input.GetMouseButtonDown(0) && planetUI.planet != planet)
			OpenPlanetUI = true;
		else if(Input.GetMouseButtonDown(0))
			OpenPlanetUI = !OpenPlanetUI;
		else if(Input.GetMouseButtonDown(1))
			OpenPlanetUI = false;
			
	}
}
