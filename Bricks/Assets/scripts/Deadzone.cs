﻿using UnityEngine;
using System.Collections;

public class Deadzone : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Ball")
		{
			GameManager.Instance.subtractLife();
		}
	}

}
