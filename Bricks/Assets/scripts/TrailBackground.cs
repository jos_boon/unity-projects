﻿using UnityEngine;
using System.Collections;

public class TrailBackground : MonoBehaviour {

	// Use this for initialization
	void Start () 
    {
        TrailRenderer t = GetComponent<TrailRenderer>();
		t.sortingLayerName = "Background";
		t.sortingOrder = 10;
	}

}
