﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour 
{
	public int HitPoints = 1;
	public int ScorePoints = 50;
	public int BallPoints = 0;
	public bool Indestructible = false;

	private SpriteRenderer spriteRend;

	void Awake()
	{
		if(!Indestructible)
			GameManager.Instance.addBrick();

		spriteRend = GetComponent<SpriteRenderer>();
	}

	void UpdateColor()
	{
		Color c = spriteRend.color;
		c.r -= .15f;
		c.g -= .15f;
		c.b -= .15f;
		spriteRend.color = c;
	}


	void OnCollisionEnter2D(Collision2D other)
	{
		if(!Indestructible && other.gameObject.tag == "Ball")
		{
			HitPoints--;

			if(HitPoints > 0)
			{
				UpdateColor();
				GameManager.Instance.addScore(BallPoints);
			}
		}
	}

	void OnCollisionExit2D(Collision2D other)
	{
		if(HitPoints <= 0)
		{
			GameManager.Instance.subtractBrick();
			GameManager.Instance.addScore(ScorePoints);
			Destroy(gameObject);
		}
	}
}
