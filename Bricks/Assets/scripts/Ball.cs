﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour 
{
	public Vector2 maxSpeed = new Vector2(1, 1);
	public bool inPlay = false;

	public Rigidbody2D rb;
	private float nudgeSpeed = 2;
	private float nudgeThreshold = 0.01f;
	private float speedThreshold = .2f;
	private AudioSource ballEffect;
	
	void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
		ballEffect = GetComponent<AudioSource>();
	}

	void Update () 
	{
		if(Input.GetButtonDown("Jump") && !inPlay)
		{
			inPlay = true;
			transform.parent = null;
			rb.isKinematic = false;
			rb.velocity = maxSpeed;
		}
	}

	void FixedUpdate()
	{
		if(inPlay)
		{
			//When the ball gets stuck in horizontal or vertical movement we give it a little nudge
			if(rb.velocity.x >= -nudgeThreshold && rb.velocity.x <= nudgeThreshold)
			{
				int n = rb.velocity.y > 0 ? 1 : -1;
				rb.velocity = rb.velocity + new Vector2(nudgeSpeed * n, 0);
			}
			else if(rb.velocity.y >= -nudgeThreshold && rb.velocity.y <= nudgeThreshold)
			{
				int n = rb.velocity.x > 0 ? 1 : -1;
				rb.velocity = rb.velocity + new Vector2(0, nudgeSpeed * n);
			}

			// Keep the ball at constant speed
			if(rb.velocity.magnitude < maxSpeed.magnitude - speedThreshold ||
			   rb.velocity.magnitude > maxSpeed.magnitude + speedThreshold)
			{
				rb.velocity = rb.velocity.normalized * maxSpeed.magnitude;
			}
		}
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		ballEffect.Play();
	}

}
