﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	private static GameManager instance;

	public int lives = 3;
	private int nBricks = 0;
	private int score = 0;

	private Transform paddle;
	private Transform ball;

	public Text txtScore;
	public Text txtLives;
	public Text txtGameStatus;
	public Button btnResume;
	public GameObject gameMenu;

	public static GameManager Instance 
	{ 
		get
		{
			if(instance == null)
				instance = FindObjectOfType<GameManager>();

			return instance;
		}
	}

	public void GameStatus()
	{
		if(nBricks <= 0)
		{
			txtGameStatus.text = "You win!";
			btnResume.interactable = false;
			PauseGame();
		}
		else if(lives < 0)
		{
			txtGameStatus.text = "Game Over!";
			btnResume.interactable = false;
			lives = 0;
			PauseGame();
		}

		txtLives.text = "Lives: " + lives.ToString();
		txtScore.text = "Score: " + score.ToString();
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.P))
			PauseGame();
	}

	public void PauseGame()
	{
		Time.timeScale = 0;
		gameMenu.SetActive(true);
	}

	public void ResumeGame()
	{
		Time.timeScale = 1;
		gameMenu.SetActive(false);
	}

	public void RestartGame()
	{
		SceneManager.LoadScene(0);
		ResumeGame();
	}
	
	public void ResetBallPosition()
	{
		Ball b = ball.GetComponent<Ball>();

		b.inPlay = false;
		b.transform.parent = paddle;
		b.rb.isKinematic = true;
		b.rb.velocity = new Vector2(0, 0);

		ball.localPosition = new Vector3(0, 0.5f, 0);
	}

	void Awake()
	{
		if(Instance != this)
			Destroy(gameObject);
		else
		{
			paddle = GameObject.Find("Paddle").transform;
			ball = GameObject.Find("Ball").transform;
		}

		GameStatus();
	}

	public void subtractLife()
	{
		lives--;
		GameStatus();
		ResetBallPosition();
	}

	public void addLife()
	{
		lives++;
		GameStatus();
	}

	public void subtractBrick()
	{
		nBricks--;
		GameStatus();
	}

	public void addBrick()
	{
		nBricks++;
	}

	public void addScore(int n)
	{
		score += n;
		GameStatus();
	}
}
