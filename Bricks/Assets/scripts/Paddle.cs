﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

	public float speed = 1f;
	private Rigidbody2D rb;

	void Awake()
	{
		rb = GetComponent<Rigidbody2D>();
	}

	void Update () 
	{
		rb.velocity = new Vector2(speed * Input.GetAxis("Horizontal"), 0);
	}
}
