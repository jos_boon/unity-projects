﻿using UnityEngine;
using System.Collections;

public class ViewBehaviour : MonoBehaviour 
{
	private float rotationX = 0f;

	public float sensitivityX = 2f;

	public void RotateX(float x)
	{
		rotationX += x * sensitivityX;
		rotationX = Mathf.Clamp (rotationX, -70, 70);
		
		transform.localEulerAngles = new Vector3(-rotationX, transform.localEulerAngles.y, transform.localEulerAngles.z);
	}
}
