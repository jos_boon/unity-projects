﻿using UnityEngine;
using System.Collections;

public class PatrolBehaviour : IAIBehaviour 
{
	private AIControls ai;

	public PatrolBehaviour(AIControls ai)
	{
		this.ai = ai;
	}

	public void UpdateBehaviour()
	{
		Debug.Log("I'm going to Patrol!");
	}
	
	public void OnTriggerBehaviour()
	{
		
	}
}
