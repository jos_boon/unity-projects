﻿using UnityEngine;
using System.Collections;

public class UserControls : MonoBehaviour {

	private MoveBehaviour move;
	private ViewBehaviour view;
	private AttackBehaviour attack;

	void Awake()
	{
		move = GetComponent<MoveBehaviour>();
		view = GetComponentInChildren<ViewBehaviour>();
		attack = GetComponent<AttackBehaviour>();
	}

	void FixedUpdate()
	{
		move.Rotate(0, Input.GetAxis("Mouse X"), 0);
		move.Move(Input.GetAxis("Horizontal"), Input.GetAxis("Jump"), Input.GetAxis("Vertical"));
		view.RotateX(Input.GetAxis("Mouse Y"));

		if(Input.GetAxis("Fire1") > 0)
			attack.Attack();
	}

}
