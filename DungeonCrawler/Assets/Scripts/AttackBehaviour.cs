﻿using UnityEngine;
using System.Collections;

public class AttackBehaviour : MonoBehaviour 
{
	public Transform rightArm;
	public float attackSpeed = 1f;

	private Vector3 baseRotation;
	public Vector3 finalRotation = new Vector3(90, 335, 0);
	private Vector3 diffRotation;

	private bool isAttacking = false;
	private float attackTimer = 0f;

	void Awake()
	{
		baseRotation = rightArm.transform.localRotation.eulerAngles;
		diffRotation = finalRotation - baseRotation;
		attackTimer = attackSpeed;
	}

	void Update()
	{
		if(isAttacking && attackTimer <= attackSpeed)
		{
			//TODO: remove Magic Numbers :D
			if(attackTimer <= attackSpeed * .5f)
				rightArm.Rotate(diffRotation / attackSpeed * 2f * Time.deltaTime);
			else
				rightArm.Rotate(-diffRotation / attackSpeed * 2f * Time.deltaTime);

			attackTimer += Time.deltaTime;
		}
		else
		{
			rightArm.localEulerAngles = baseRotation;
			isAttacking = false;
		}
	}

	public void Attack()
	{
		if(!isAttacking)
		{
			isAttacking = true;
			attackTimer = 0f;
		}
			
	}
}
