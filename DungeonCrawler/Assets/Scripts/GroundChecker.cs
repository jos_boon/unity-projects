﻿using UnityEngine;
using System.Collections;

public class GroundChecker : MonoBehaviour 
{
	private MoveBehaviour move;

	void Awake()
	{
		move = transform.parent.GetComponent<MoveBehaviour>();
	}

	void OnTriggerEnter(Collider other)
	{
		move.IsOnGround = true;
	}

	void OnTriggerExit(Collider other)
	{
		move.IsOnGround = false;
	}
}
