﻿using UnityEngine;
using System.Collections;

public class MoveBehaviour : MonoBehaviour {

	public float movementSpeed = 1;
	public float rotateSpeed = 1;
	public float jumpForce = 1000;
	private Rigidbody rb;

	private bool justJumped = false;
	private bool isOnGround = true;

	public bool IsOnGround
	{
		get
		{
			return isOnGround;
		}
		set
		{
			isOnGround = value;
		}
	}

	void Awake()
	{
		rb = GetComponent<Rigidbody>();
	}

	void OnEnable()
	{
		Debug.Log("Ignoring collision");
		Physics.IgnoreCollision(GetComponent<Collider>(), transform.FindChild("FeetSensor").GetComponent<Collider>());
		Physics.IgnoreCollision(GetComponent<Collider>(), transform.FindChild("RightArm").FindChild("Weapon").GetComponent<Collider>());
	}

	public void Move(float x, float y, float z)
	{
		if(!justJumped && IsOnGround && y > 0)
		{
			justJumped = true;
			rb.AddForce(new Vector3(0, y * jumpForce, 0) );
		}
		else
			justJumped = false;

		Vector3 movement = transform.forward * z + transform.right * x;

		movement = movement.normalized * movementSpeed * Time.deltaTime;

		rb.MovePosition(transform.position + movement);
	}

	public void Rotate(float x, float y, float z)
	{
		Quaternion deltaRotation = Quaternion.Euler(new Vector3(x, y, z) * rotateSpeed * Time.deltaTime);
		rb.MoveRotation(rb.rotation * deltaRotation);
	}
}
