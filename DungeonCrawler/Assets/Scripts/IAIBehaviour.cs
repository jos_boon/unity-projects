﻿using UnityEngine;
using System.Collections;

public interface IAIBehaviour 
{
	void UpdateBehaviour();
	void OnTriggerBehaviour();
}
