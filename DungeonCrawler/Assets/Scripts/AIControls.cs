﻿using UnityEngine;
using System.Collections;

public class AIControls : MonoBehaviour {

	[HideInInspector]
	public IAIBehaviour currentBehaviour;
	[HideInInspector]
	public SearchBehaviour searchBehaviour;
	[HideInInspector]
	public PatrolBehaviour patrolBehaviour;
	[HideInInspector]
	public ChaseBehaviour chaseBehaviour;

	void Awake()
	{
		patrolBehaviour = new PatrolBehaviour(this);


		currentBehaviour = patrolBehaviour;
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		currentBehaviour.UpdateBehaviour();
	}
}
